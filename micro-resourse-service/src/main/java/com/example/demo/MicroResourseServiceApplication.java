package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@EnableResourceServer
public class MicroResourseServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroResourseServiceApplication.class, args);
	}

	@RequestMapping(value = "/customers")
	public List<Customer> getCustomer() {
		List<Customer> custList = new ArrayList<Customer>();
		
		Customer cust = new Customer("4522","Robert");
		custList.add(cust);
		return custList;
	}

	public class Customer {
		public String custId;
		public String custName;

		public Customer() {

		}

		public Customer(String custId,String custName) {
			this.custId = custId;
			this.custName = custName;
		}
	}
}
